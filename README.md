# Pet project (online-shop) on PHP
Project is developed on simple MVC carcass.

# System requirements
- Vagrant
- Virtual Box

# Deploying via Vagrant
In project folder, open CLI and enter: 
 `vagrant up`. <br>

# Configuring project
The project settings are stored in file `script.sh`.
<br><br>
Config variables are look something like this:
<br>

| Database settings | Xdebug settings | Virtual host settings |
| ------ | ------ | ------ |
| DBHOST=localhost | XDEBUG_KEY=PHPSTORM | VH_SERVERNAME=kompshop.ru |
| DBNAME=kompshop | XDEBUG_HOST_IP=192.168.41.201  | VH_DOCROOT=/var/www/kompshop.ru 
| DBUSER=root | XDEBUG_PORT=9000 | VH_DBFILE=VH_DOCROOT/kompshop.sql
| DBPASSWD=root |