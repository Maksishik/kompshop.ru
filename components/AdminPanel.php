<?php

/**
 * Abstract class AdminBase, that contains general logic for controllers that are used in the admin panel
 * Абстрактный класс AdminBase содержит общую логику для контроллеров, которые используются в панели администратора
 */
abstract class AdminPanel
{
    public function __construct()
    {
        self::checkAdmin();
    }

    /**
     * Check user for admin rules
     * Метод, который проверяет пользователя на то, является ли он администратором
     * @return boolean
     */
    public static function checkAdmin()
    {
        // Is user authorized
        // Проверяем авторизирован ли пользователь. Если нет, он будет переадресован
        $userId = User::checkLogged();

        // Get user info
        // Получаем информацию о текущем пользователе
        $user = User::getUserById($userId);

        // Is user an admin
        // Если роль текущего пользователя "admin", пускаем его в админпанель
        if ($user['role'] == 'admin') {
            return true;
        }

        header('Location: /');
        //die('Access denied');
    }
}
