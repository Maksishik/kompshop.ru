<?php

/*
 * Class Pagination for generating page navigation <br>
 * Класс для генерации постраничной навигации
 */
class Pagination
{
    /**
     * Max count of links  on the current page <br>
     * Ссылок навигации на страницу
     * @var int
     */
    private $max = 10;

    /**
     * The GET method key, which store page number <br>
     * Ключ для GET, в который пишется номер страницы
     * @var string
     */
    private $index = 'page';

    /**
     * Current page <br>
     * Текущая страница
     * @var int
     */
    private $current_page;

    /**
     * Total records count <br>
     * Общее количество записей
     * @var
     */
    private $total;

    /**
     * Max count of record on the page <br>
     * Записей на страницу
     * @var int
     */
    private $limit;

    /**
     * @param integer $total <p>total records count <br> общее количество записей</p>
     * @param integer $limit <p>max count of record on the page <br> количество записей на страницу</p>
     * @param integer $currentPage <p>current page number <br> номер текущей страницы</p>
     * @param string $index <p>url-key <br> url-ключ</p>
     */
    public function __construct($total, $currentPage, $limit, $index)
    {
        $this->total = $total;
        $this->limit = $limit;
        $this->index = $index;
        $this->amount = $this->amount();
        $this->setCurrentPage($currentPage);
    }

    /**
     * Link generation  <br>
     * Для вывода ссылок
     * @return HTML-code with nav link <br> HTML-код со ссылками навигации
     */
    public function get()
    {
        // variable for storing links <br> для записи ссылок
        $links = null;

        // loop limit <br> получаем ограничения для цикла
        $limits = $this->limits();

        $html = '<ul class="pagination">';

        // generating links <br> генерируем ссылки
        for ($page = $limits[0]; $page <= $limits[1]; $page++) {
            // if current_page = $page - dont generate link and add active class <br>
            // eсли страница является текущей страница, то убирается ссылка и добавляется класс active
            if ($page == $this->current_page) {
                $links .= '<li class="active"><a href="//">' . $page . '</a></li>';
            } else {
                // Else generating link <br> иначе генерируем ссылку
                $links .= $this->generateHtml($page);
            }
        }

        // If links are created <br>
        // Если ссылки создались
        if (!is_null($links)) {
            // Current page are note first page <br>
            // Если текущая страница не первая
            if ($this->current_page > 1)
                // Generate link to main page <br>
                // Создаём ссылку "На первую"
                $links = $this->generateHtml(1, '&lt;') . $links;

            // Current page are note first page <br>
            // Если текущая страница не первая
            if ($this->current_page < $this->amount)
                // Generate link to last page <br>
                // Создаём ссылку "На последнюю"
                $links .= $this->generateHtml($this->amount, '&gt;');
        }

        $html .= $links . '</ul>';

        // Return html <br>
        // Возвращаем html
        return $html;
    }

    /**
     * Для генерации HTML-кода ссылки
     * @param integer $page <p>номер страницы</p>
     * @param null $text
     * @return string <p>HTML код ссылки</p>
     */
    private function generateHtml($page, $text = null)
    {
        // If link text is empty <br>
        // Если текст ссылки не указан
        if (!$text)
            // Text is the page number <br>
            // Указываем, что текст - цифра страницы
            $text = $page;

        $currentURI = rtrim($_SERVER['REQUEST_URI'], '/') . '/';
        $currentURI = preg_replace('~/page-[0-9]+~', '', $currentURI);

        // Generating HTML links code <br>
        // Формируем HTML код ссылки и возвращаем
        return
            '<li><a href="' . $currentURI . $this->index . $page . '">' . $text . '</a></li>';
    }

    /**
     *
     * Для получения, откуда стартовать
     * @return <p>массив с началом и концом отсчёта</p>
     */
    private function limits()
    {
        // Вычисляем ссылки слева (чтобы активная ссылка была посередине)
        $left = $this->current_page - round($this->max / 2);

        // Вычисляем начало отсчёта
        $start = $left > 0 ? $left : 1;

        // Если впереди есть как минимум $this->max страниц
        if ($start + $this->max <= $this->amount)
            // Назначаем конец цикла вперёд на $this->max страниц или просто на минимум
            $end = $start > 1 ? $start + $this->max : $this->max;
        else {
            // Конец - общее количество страниц
            $end = $this->amount;

            // Начало - минус $this->max от конца
            $start = $this->amount - $this->max > 0 ? $this->amount - $this->max : 1;
        }

        // Возвращаем
        return
            array($start, $end);
    }

    /**
     * Set current page <br>
     * Для установки текущей страницы
     */
    private function setCurrentPage($currentPage)
    {
        // Получаем номер страницы
        $this->current_page = $currentPage;

        // Если текущая страница боле нуля
        if ($this->current_page > 0) {
            // Если текунщая страница меньше общего количества страниц
            if ($this->current_page > $this->amount)
                // Устанавливаем страницу на последнюю
                $this->current_page = $this->amount;
        } else
            // Устанавливаем страницу на первую
            $this->current_page = 1;
    }

    /**
     * Get total number of page <br>
     * Для получеия общего числа страниц
     * @return <p>число страниц</p>
     */
    private function amount()
    {
        // Делим и возвращаем
        return round($this->total / $this->limit);
    }
}