<?php

/**
 * Class Router call controller`s action by url rules in routes.php file <br>
 * Класс Роутер определяет по url - какой контроллер и action вызвать, используя маршруты прописанные в файале routes.php
 */
class Router
{
    /**
     * Routes list <br>
     * Cписок маршрутов
     * @var array
     */
	private $routes;

	public function __construct()
	{
		$routesPath = ROOT.'/config/routes.php';
		$this->routes = include($routesPath);
	}

    /**
     * Get user`s request uri <br>
     * Получение uri запроса пользователя
     * @return string
     */
	private function getURI()
	{
		if (!empty($_SERVER['REQUEST_URI'])) {
		    return trim($_SERVER['REQUEST_URI'], '/');
		}
	}

    /**
     * Running controller`s action <br>
     * Запуск action-a контроллера
     */
	public function run()
	{
		$uri = $this->getURI();

		foreach ($this->routes as $uriPattern => $path) {
		    // Check match uri and route`s list pattern <br>
            // Проверка на совпадение uri и паттерна из списка маршрутов
			if(preg_match("~$uriPattern~", $uri)) {
				// Get internal route from outer by rule <br>
                // Получаем внутренний путь из внешнего согласно правилу
				$internalRoute = preg_replace("~$uriPattern~", $path, $uri);

				// Explode uri for get params, controller and action names <br>
                // Разделение Uri дял получения парметров и имен контроллера и action
				$segments = explode('/', $internalRoute);

				$controllerName = array_shift($segments).'Controller';
				$controllerName = ucfirst($controllerName);

				$actionName = 'action'.ucfirst(array_shift($segments));

				$parameters = $segments;

				// Get controller file name <br>
                // Полоучение имена файла контроллера
				$controllerFile = ROOT . '/controllers/' .$controllerName. '.php';
				if (file_exists($controllerFile)) {
					include_once($controllerFile);
				}

				$controllerObject = new $controllerName;

                try {
                    // Вызов action <br>
                    // Calling action
                    $result = call_user_func_array(array($controllerObject, $actionName), $parameters);

                    // If math->break loop <br>
                    // Закончить цикл, если нашло совпадение
                    if ($result != null) {
                        break;
                    } else {
                        throw new \RuntimeException("call_user_func_array failed");
                    }
                } catch (Exception $exception) {
                    header('Location: /');
                }
			}
		}
	}
}