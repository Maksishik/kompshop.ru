<?php

/**
 * Class for saving user`s messages <br>
 * Класс для сохранения пользовательских сообщений с сайта
 */
class UserMessages
{
    public static function sendMessage($userMail, $msg) {
        $db = Db::getConnection();

        $sql = "insert into user_messages (user_email, msg, notification_type_id) values (:userMail, :msg, 2)";

        $result = $db->prepare($sql);
        $result->bindParam(':userMail', $userMail, PDO::PARAM_STR);
        $result->bindParam(':msg', $msg, PDO::PARAM_STR);

        $result = $result->execute();

        return $result;
    }
}
