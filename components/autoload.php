<?php
/**
 * autoload classes from the different folders
 */
spl_autoload_register(function($class_name)
{
    $array_paths = array (
        '/components/',
        '/models/',
        '/parser/',
        '/controllers/admin/',
        '/book/refactoring.guru/'
    );

    foreach ($array_paths as $path) {
        $path = ROOT . $path . $class_name . '.php';

        if (is_file($path)) {
            include $path;
        }
    }
});
