<?php
return array(
    // Товар
    'product/([0-9]+)' => 'product/view/$1',    // actionView in ProductController

    // Каталог
    'catalog' => 'catalog/index',   // actionIndex in CatalogController

    // Категория товаров
    'category/([0-9]+)/page-([0-9]+)' => 'catalog/category/$1/$2',  // actionCategory in CategoryCatalog
    'category/([0-9]+)' => 'catalog/category/$1',   // actionCategory in CategoryCatalog

    // Корзина
    'cart/add/([0-9]+)' => 'cart/add/$1',   // actionAdd in CartController
    'cart/remove/([0-9]+)' => 'cart/remove/$1', // actionRemove in CartController
    'cart/checkout' => 'cart/checkout', // actionCheckout in CartController
    'cart' => 'cart/index', // actionIndex in CartController

    // Пользователь
    'user/registration' => 'user/registration', //actionRegistration in UserController
    'user/login' => 'user/login',   // actionLogin in UserController
    'user/logout' => 'user/logout', // actionLogout in UserController
    'cabinet/edit' => 'cabinet/edit',   // actionEdit in CabinetController
    'cabinet/history' => 'cabinet/history', // actionHistory in CabinetController
    'cabinet' => 'cabinet/index',   // actionIndex in CabinetController

    // Админка
    // Управление товарами:
    'admin/product/create' => 'adminProduct/create',    // actionCreate in AdminProductController
    'admin/product/update/([0-9]+)' => 'adminProduct/update/$1',    // actionUpdate in AdminProductController
    'admin/product/delete/([0-9]+)' => 'adminProduct/delete/$1',    // actionDelete in AdminProductController
    'admin/product' => 'adminProduct/index',    // actionIndex in AdminProductController
    // Управление категориями:
    'admin/category/create' => 'adminCategory/create',  // actionCreate in AdminCategoryController
    'admin/category/update/([0-9]+)' => 'adminCategory/update/$1',  // actionUpdate in AdminCategoryController
    'admin/category/delete/([0-9]+)' => 'adminCategory/delete/$1',  // actionDelete in AdminCategoryController
    'admin/category' => 'adminCategory/index',  // actionIndex in AdminCategoryController
    // Управление заказами:
    'admin/order/update/([0-9]+)' => 'adminOrder/update/$1',    // actionUpdate in AdminOrderController
    'admin/order/delete/([0-9]+)' => 'adminOrder/delete/$1',    // actionUpdate in AdminOrderController
    'admin/order/view/([0-9]+)' => 'adminOrder/view/$1',    // actionUpdate in AdminOrderController
    'admin/order' => 'adminOrder/index',    // actionIndex in AdminOrderController
    // Админпанель:
    'admin' => 'admin/index',   // actionIndex in AdminController

    // Новостная лента
    'blog/([0-9]+)'=>'news/view/$1',    // actionView in BlogController
    'blog'=>'news/index',   // actionIndex in BlogController

	// Форма обратной связи
	'contacts' => 'site/contact', // actionContact in SiteController

    // Главная страница
    'index.php' => 'site/index', // actionIndex в SiteController
    '' => 'site/index', // actionIndex в SiteController
);
