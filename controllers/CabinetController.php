<?php

/**
 * User cabinet <br>
 * Кабинет пользователя
 */
class CabinetController
{
    /**
     * Action for user cabinet page <br>
     * Action для страницы "Кабинет пользователя"
     */
    public function actionIndex()
    {
        // Get user id from session <br>
        // Получаем идентификатор пользователя из сессии
        $userId = User::checkLogged();

        $user = User::getUserById($userId);

        require_once ROOT .'/views/cabinet/index.php';
        return true;
    }

    /**
     * Action for editing user data <br>
     * Action для страницы "Редактирование данных пользователя"
     */
    public function actionEdit()
    {
        // Get user id from session <br>
        // Получаем идентификатор пользователя из сессии
        $userId = User::checkLogged();

        $user = User::getUserById($userId);

        $name = $user['name'];
        $password = $user['password'];

        // Form result flag <br>
        // Флаг результата формы
        $result = false;

        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $password = $_POST['password'];

            // Form error flag <br>
            // Флаг ошибок в форме
            $errors = false;

            if (!User::checkName($name)) {
                $errors[] = 'Имя не должно быть короче двух символов';
            }

            if (!User::checkPassword($password)) {
                $errors[] = 'Пароль не должен быть короче 6-ти символов';
            }

            if ($errors == false) {
                $result = User::edit($userId, $name, $password);
            }
        }

        require_once ROOT . '/views/cabinet/edit.php';
        return true;
    }

    /**
     * Action for user orders history <br>
     * Action для страницы "История заказов пользователя"
     */
    public function actionHistory()
    {
        // Get user id from session <br>
        // Получаем идентификатор пользователя из сессии
        $userId = User::checkLogged();

        $user = User::getUserById($userId);

        $name = $user['name'];
        $password = $user['password'];

        $result = Order::getUsersOrderList($userId);

        require_once ROOT . '/views/cabinet/order_list.php';
        return true;
    }
}
