<?php

/**
 * Cart
 * Корзина
 */
class CartController
{
    /**
     * Action for adding products in cart by ajax <br>
     * Action для добавления товара в корзину при помощи асинхронного запроса (ajax)
     * @param integer $id <p>id товара</p>
     */
    public function actionAdd($id) {
        echo Cart::addProduct($id);

        return true;
    }

    /**
     * Action for remove products from the cart <br>
     * Action для удаления товара из корзины
     * @param integer $id <p>id товара</p>
     */
    public function actionRemove($id) {
        Cart::removeProduct($id);

        header('Location: /cart/');
    }

    /**
     * Action for checkout <br>
     * Action для страницы "Оформление покупки"
     */
    public function actionCheckout()
    {
        $categories = Category::getCategoriesList();
        $productsInCart = Cart::getProducts();

        // Redirect user <br>
        // Если товаров нет, отправляем пользователи искать товары на главную
        if ($productsInCart == false) {
            header("Location: /");
        }

        // Getting total cost <br>
        // Находим общую стоимость
        $productsIds = array_keys($productsInCart);
        $products = Product::getProdustsByIds($productsIds);
        $totalPrice = Cart::getTotalPrice($products);

        $totalQuantity = Cart::countItems();

        $userName = false;
        $userPhone = false;
        $userComment = false;

        // Successful checkout status <br>
        // Успешный статус оформления заказа
        $result = false;

        // Check user for authorization <br>
        // Проверяем является ли пользователь гостем
        if (!User::isGuest()) {
            $userId = User::checkLogged();
            $user = User::getUserById($userId);
            $userName = $user['name'];
        } else {
            // Если гость, поля формы останутся пустыми
            $userId = false;
        }

        if (isset($_POST['submit'])) {
            $userName = $_POST['userName'];
            $userPhone = $_POST['userPhone'];
            $userComment = $_POST['userComment'];

            // Form error flag <br>
            // Флаг ошибок в форме
            $errors = false;

            if (!User::checkName($userName)) {
                $errors[] = 'Неправильное имя';
            }
            if (!User::checkPhone($userPhone)) {
                $errors[] = 'Неправильный телефон';
            }

            if ($errors == false) {
                $result = Order::saveOrder($userName, $userPhone, $userComment, $userId, $productsInCart, Order::generateOrderNumber());

                if ($result) {
                    Cart::clear();
                }
            }
        }

        require_once(ROOT . '/views/cart/checkout.php');
        return true;
    }

    /**
     * Action for cart page <br>
     * Action для страницы "Корзина"
     */
    public function actionIndex() {
        $categories = array();
        $categories = Category::getCategoriesList();

        $productsInCart = false;
        $productsInCart = Cart::getProducts();

        if ($productsInCart) {
            $productsIds = array_keys($productsInCart);
            $products = Product::getProductByIds($productsIds);

            $totalPrice = Cart::getTotalPrice($products);
        }

        require_once ROOT . '/views/cart/index.php';
        return true;
    }
}
