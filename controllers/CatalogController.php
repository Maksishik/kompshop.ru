<?php

/**
 * Product catalog <br>
 * Каталог товаров
 */
class CatalogController {
    /**
     * Action for products catalog page <br>
     * Action для страницы "Каталог товаров"
     */
    public function actionIndex() {
        $categories = array();
        $categories = Category::getCategoriesList();

        $latestProducts = array();
        $latestProducts = Product::getLatestProducts();

        include_once ROOT . '/views/catalog/index.php';
        return true;
    }

    /**
     * Action for product categories page <br>
     * Action для страницы "Категория товаров"
     * @param int $categoryId <p>id категории</p>
     * @param int $page <p>номер страницы</p>
     */
    public function actionCategory($categoryId, $page = 1) {
        $categories = array();
        $categories = Category::getCategoriesList();

        $categoryProducts = array();
        $categoryProducts = Product::getProductsListByCategory($categoryId, $page);

        // Общее количество товаров (необходимо для постраничной навигации)
        $total = Product::getTotalProductsInCategory($categoryId);

        // Создаем объект Pagination - постраничная навигация
        $pagination = new Pagination($total, $page, Product::SHOW_BY_DEFAULT, 'page-');

        require_once ROOT.'/views/catalog/category.php';
        return true;
    }
}
