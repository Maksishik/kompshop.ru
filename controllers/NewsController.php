<?php

/**
 * Site news <br>
 * Новостная лента сайта
 */
class NewsController
{
    /**
     * Action for news page <br>
     * Action для страницы "Новости"
     */
    public function actionIndex()
    {
        $newsList = array();
        $newsList = News::getNewsList();

        include_once ROOT.'/views/blog/index.php';
        return true;
    }

    /**
     * Action for viewing news page <br>
     * Action для страницы "Просмотр одной новости"
     * @param int $id <p>id новости</p>
     */
    public function actionView($id)
    {
        $categories = array();
        $categories = Category::getCategoriesList();

        $newsList = array();
        $newsList = News::getNewsItemById($id);

        include_once ROOT.'/views/blog/view.php';
        return true;
    }
}
