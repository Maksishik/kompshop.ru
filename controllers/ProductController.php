<?php

/**
 * Product controller <br>
 * Товар
 */
class ProductController {
    /**
     * Action для страницы просмотра товара
     * @param int $productId <p>id товара</p>
     */
    public function actionView($productId) {
        $categories = array();
        $categories = Category::getCategoriesList();

        $product = Product::getProductById($productId);

        include_once ROOT.'/views/product/view.php';
        return true;
    }
}
