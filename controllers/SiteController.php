<?php

/**
 * Site main page
 * Главная страница сайта
 */
class SiteController {
    /**
     * Action for main page <br>
     * Action для главной страницы
     */
    public function actionIndex() {
        $categories = array();
        $categories = Category::getCategoriesList();

        $latestProducts = array();
        $latestProducts = Product::getLatestProducts();

        $recommendedProducts = array();
        $recommendedProducts = Product::getRecommendedProducts();

        include_once ROOT.'/views/site/index.php';
        return true;
    }

    /**
     * Action for callback page
     * Action для страницы "Контакты"
     */
    public function actionContact()
    {
        $userMail = '';
        $userMsg = '';

        // Form result flag <br>
        // Флаг результата формы
        $result = false;

        if (isset($_POST['submit'])) {
            $userMail = $_POST['email'];
            $userMsg = $_POST['msg'];

            // Form error flag <br>
            // Флаг ошибок в форме
            $errors = false;

            if (!User::checkEmail($userMail)) {
                $errors[] = 'Непарвильный email';
            }

            if ($errors == false) {
                $result = UserMessages::sendMessage($userMail, $userMsg);
            }
        }

        require_once ROOT.'/views/site/contacts.php';
        return true;
    }
}
