<?php

/**
 * Class UserController
 */
class UserController
{
    /**
     * Action for user registration <br>
     * Action для страницы "Регистрация"
     */
    public function actionRegistration()
    {
        $name = '';
        $email = '';
        $password = '';
        $result = false;

        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $email = $_POST['email'];
            $password = $_POST['password'];

            // Form error flag <br>
            // Флаг ошибок в форме
            $errors = false;

            if (!User::checkName($name)) {
                $errors[] = 'Имя не должно быть короче двух символов';
            }

            if (!User::checkEmail($email)) {
                $errors[] = 'Неправильный email';
            }

            if (!User::checkPassword($password)) {
                $errors[] = 'Пароль не должен быть короче шести символов';
            }

            if (User::checkEmailExists($email)) {
                $errors[] = 'Такой email уже используется';
            }

            if (!$errors) {
                $result = User::registration($name, $email, $password);
            }
        }

        require_once(ROOT . '/views/user/registration.php');
        return true;
    }

    /**
     * Action for login page <br>
     * Action для страницы "Вход на сайт"
     */
    public function actionLogin()
    {
        $email = '';
        $password = '';

        if (isset($_POST['submit'])) {
            $email = $_POST['email'];
            $password = $_POST['password'];

            $errors = false;

            if (!User::checkEmail($email)) {
                $errors[] = 'Непарвильный email';
            }

            if (!User::checkPassword($password)) {
                $errors[] = 'Пароль не должен быть короче 6-ти символов';
            }

            $userId = User::checkUserData($email, $password);

            if ($userId == false) {
                $errors[] = 'Логин или пароль не правильные';
            } else {
                User::auth($userId);

                header("Location: /cabinet/");
                exit;
            }
        }

        require_once ROOT.'/views/user/login.php';
        return true;
    }

    /**
     * Removing user data from session <br>
     * Удаляем данные о пользователе из сессии
     */
    public function actionLogout()
    {
        unset($_SESSION['user']);

        header("Location: /");
        exit;
    }
}
