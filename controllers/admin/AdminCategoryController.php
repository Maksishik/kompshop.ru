<?php

/**
 * Category management in admin panel <br>
 * Управление категориями в админпанели
 */
class AdminCategoryController extends AdminPanel
{
    /**
     * Action for categories management <br>
     * Action для страницы "Управление категориями"
     */
    public function actionIndex()
    {
        $categoriesList = Category::getCategoriesListAdmin();

        require_once(ROOT . '/views/admin_category/index.php');
        return true;
    }

    /**
     * Action for adding categories <br>
     * Action для страницы "Добавить категорию"
     */
    public function actionCreate()
    {
        // Form processing <br>
        // Обработка формы
        if (isset($_POST['submit'])) {
            // Getting form data <br>
            // Если форма отправлена получаем данные из формы
            $name = $_POST['name'];
            $sortOrder = $_POST['sort_order'];
            $status = $_POST['status'];

            // Form error flag <br>
            // Флаг ошибок в форме
            $errors = false;

            if (!isset($name) || empty($name)) {
                $errors[] = 'Заполните поля';
            }

            if ($errors == false) {
                Category::createCategory($name, $sortOrder, $status);

                header("Location: /admin/category");
            }
        }

        require_once(ROOT . '/views/admin_category/create.php');
        return true;
    }

    /**
     * Action for edit category <br>
     * Action для страницы "Редактировать категорию"
     * @param int $id <p>id категории</p>
     */
    public function actionUpdate($id)
    {
        // Get info about one category <br>
        // Получаем данные о конкретной категории
        $category = Category::getCategoryById($id);

        // Form processing <br>
        // Обработка формы
        if (isset($_POST['submit'])) {
            // Getting form data <br>
            // Если форма отправлена получаем данные из формы
            $name = $_POST['name'];
            $sortOrder = $_POST['sort_order'];
            $status = $_POST['status'];

            Category::updateCategoryById($id, $name, $sortOrder, $status);

            header("Location: /admin/category");
        }

        require_once(ROOT . '/views/admin_category/update.php');
        return true;
    }

    /**
     * Action for delete category <br>
     * Action для страницы "Удалить категорию"
     * @param int $id <p>id категории</p>
     */
    public function actionDelete($id)
    {
        // Form processing <br>
        // Обработка формы
        if (isset($_POST['submit'])) {
            Category::deleteCategoryById($id);

            header("Location: /admin/category");
        }

        require_once(ROOT . '/views/admin_category/delete.php');
        return true;
    }
}