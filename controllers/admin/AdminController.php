<?php

/**
 * Admin panel Main page <br>
 * Главная страница в админпанели
 */
class AdminController extends AdminPanel
{
    /**
     * Admin panel controller
     * Action для стартовой страницы "Панель администратора"
     */
    public function actionIndex()
    {
        require_once(ROOT . '/views/admin/index.php');
        return true;
    }

}
