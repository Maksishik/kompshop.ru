<?php

/**
 * Order management in admin panel <br>
 * Управление заказами в админпанели
 */
class AdminOrderController extends AdminPanel
{
    /**
     * Action for orders management <br>
     * Action для страницы "Управление заказами"
     */
    public function actionIndex()
    {
        $ordersList = Order::getOrdersList();

        require_once(ROOT . '/views/admin_order/index.php');
        return true;
    }

    /**
     * Action for edit categories <br>
     * Action для страницы "Редактирование заказа"
     * @param int $id <p>id заказа</p>
     */
    public function actionUpdate($id)
    {
        $order = Order::getOrderById($id);

        // Form processing <br>
        // Обработка формы
        if (isset($_POST['submit'])) {
            $userName = $_POST['userName'];
            $userPhone = $_POST['userPhone'];
            $userComment = $_POST['userComment'];
            $date = $_POST['date'];
            $status = $_POST['status'];

            // Saving changes <br>
            // Сохраняем изменения
            Order::updateOrderById($id, $userName, $userPhone, $userComment, $date, $status);

            // Redirect user <br>
            // Перенаправляем пользователя на страницу управлениями заказами
            header("Location: /admin/order/view/$id");
        }

        require_once(ROOT . '/views/admin_order/update.php');
        return true;
    }

    /**
     * Action for viewing orders <br>
     * Action для страницы "Просмотр заказа"
     * @param int $id <p>id заказа</p>
     */
    public function actionView($id)
    {
        $order = Order::getOrderById($id);

        // Getting array with ids and products count <br>
        // Получаем массив с идентификаторами и количеством товаров
        $productsQuantity = json_decode($order['products'], true);

        // Products ids <br>
        // Получаем массив с индентификаторами товаров
        $productsIds = array_keys($productsQuantity);

        // Order`s products <br>
        // Получаем список товаров в заказе
        $products = Product::getProdustsByIds($productsIds);

        require_once(ROOT . '/views/admin_order/view.php');
        return true;
    }

    /**
     * Action for delete order <br>
     * Action для страницы "Удалить заказ"
     * @param int $id <p>id заказа</p>
     */
    public function actionDelete($id)
    {
        // Form processing <br>
        // Обработка формы
        if (isset($_POST['submit'])) {
            Order::deleteOrderById($id);

            // Redirect user <br>
            // Перенаправляем пользователя на страницу управлениями товарами
            header("Location: /admin/order");
        }

        require_once(ROOT . '/views/admin_order/delete.php');
        return true;
    }
}
