<?php

/**
 * Product management in admin panel <br>
 * Управление товарами в админпанели
 */
class AdminProductController extends AdminPanel
{
    /**
     * Action for products management <br>
     * Action для страницы "Управление товарами"
     */
    public function actionIndex()
    {
        $productsList = Product::getProductsList();

        require_once ROOT . '/views/admin_product/index.php';
        return true;
    }

    /**
     * Action for adding product <br>
     * Action для страницы "Добавить товар"
     */
    public function actionCreate()
    {
        // Getting categories list for select tag <br>
        // Получаем список категорий для выпадающего списка
        $categoriesList = Category::getCategoriesListAdmin();

        // Form processing <br>
        // Обработка формы
        if (isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['code'] = $_POST['code'];
            $options['price'] = $_POST['price'];
            $options['category_id'] = $_POST['category_id'];
            $options['brand'] = $_POST['brand'];
            $options['availability'] = $_POST['availability'];
            $options['description'] = $_POST['description'];
            $options['is_new'] = $_POST['is_new'];
            $options['is_recommended'] = $_POST['is_recommended'];
            $options['status'] = $_POST['status'];

            // Form error flag <br>
            // Флаг ошибок в форме
            $errors = false;

            if(!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заполните поля';
            }

            if ($errors == false) {
                $id = Product::createProduct($options);

                if ($id) {
                    // Check for upload image <br>
                    // Проверим, загружалось ли через форму изображение
                    if (is_uploaded_file($_FILES['image']['tmp_name'])) {
                        // Move upload image with new name <br>
                        // Если загружалось, переместим его в нужную папке, дадим новое имя
                        move_uploaded_file($_FILES['image']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . "/upload/images/");
                    }
                }

                // Redirect user <br>
                // Перенаправляем пользователя на страницу управлениями товарами
                header('Location: /admin/product');
            }
        }

        require_once ROOT . '/views/admin_product/create.php';
        return true;
    }

    /**
     * Action for edit product <br>
     * Action для страницы "Редактировать товар"
     * @param int $id <p>id товара</p>
     */
    public function actionUpdate($id)
    {
        // Getting categories list for select tag <br>
        // Получаем список категорий для выпадающего списка
        $categoriesList = Category::getCategoriesListAdmin();

        // Get order data <br>
        // Получаем данные о конкретном заказе
        $product = Product::getProductById($id);

        // Form processing <br>
        // Обработка формы
        if (isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['code'] = $_POST['code'];
            $options['price'] = $_POST['price'];
            $options['category_id'] = $_POST['category_id'];
            $options['brand'] = $_POST['brand'];
            $options['availability'] = $_POST['availability'];
            $options['description'] = $_POST['description'];
            $options['is_new'] = $_POST['is_new'];
            $options['is_recommended'] = $_POST['is_recommended'];
            $options['status'] = $_POST['status'];

            // Saving changes <br>
            // Сохраняем изменения
            if (Product::updateProductById($id, $options)) {
                // Check for upload image <br>
                // Проверим, загружалось ли через форму изображение
                if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                    // Move upload image with new name <br>
                    // Если загружалось, переместим его в нужную папке, дадим новое имя
                    move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/upload/images/products/{$id}.jpg");
                }
            }

            // Redirect user <br>
            // Перенаправляем пользователя на страницу управлениями товарами
            header("Location: /admin/product");
        }

        require_once(ROOT . '/views/admin_product/update.php');
        return true;
    }

    /**
     * Action for delete product <br>
     * Action для страницы "Удалить товар"
     * @param int $id <p>id товара</p>
     */
    public function actionDelete($id)
    {
        if (isset($_POST['submit'])) {
            Product::deleteProductById($id);

            header('Location: /admin/product');
        }

        require_once ROOT . '/views/admin_product/delete.php';
        return true;
    }
}