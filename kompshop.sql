-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Апр 14 2021 г., 14:32
-- Версия сервера: 5.7.33-0ubuntu0.16.04.1
-- Версия PHP: 7.0.33-0ubuntu0.16.04.16


SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `kompshop`
--

-- --------------------------------------------------------

--
-- Структура таблицы `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `short_content` text NOT NULL,
  `full_content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `blog`
--

INSERT INTO `blog` (`id`, `title`, `short_content`, `full_content`) VALUES
(1, 'Test title', 'Test short content', 'Test full content'),
(2, 'Test title 2', 'Test short content 2', 'Test full content 2');

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `name`, `sort_order`, `status`) VALUES
(1, 'Мониторы', 0, 1),
(2, 'Ноутбуки', 0, 1),
(3, 'Видеокарты', 0, 1),
(4, 'Материнские платы', 0, 1),
(5, 'Жесткие диски SSD', 0, 1),
(6, 'Процессоры', 0, 1),
(7, 'Оперативная Память', 0, 1),
(8, 'Блоки Питания', 0, 1),
(9, 'Корпуса', 0, 1),
(10, 'Сетевое оборудование', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'Наименование',
  `category_id` int(11) NOT NULL COMMENT 'Категория',
  `code` text NOT NULL COMMENT 'Код товара',
  `price` float NOT NULL COMMENT 'Стоимость',
  `availability` int(11) NOT NULL COMMENT 'Доступность (под заказ или есть в наличии)',
  `brand` varchar(255) NOT NULL COMMENT 'Производитель',
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL COMMENT 'Описание',
  `is_new` int(11) NOT NULL DEFAULT '0',
  `is_recommended` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`id`, `name`, `category_id`, `code`, `price`, `availability`, `brand`, `image`, `description`, `is_new`, `is_recommended`, `status`) VALUES
(1, 'Acer 24" RG241YPbiipx Nitro', 1, 'UM.QR1EE.P01', 16500, 1, 'Acer ', '', '23.8", IPS, 1920x1080 (Full HD), 1 мс, 165 Гц, AMD FreeSync Premium, 250 кд/м2, 178°/178°, 2xHDMI, DisplayPort, чёрный', 1, 1, 1),
(2, 'Процессор AMD Ryzen 5 3600 OEM', 6, '100-000000031', 14900, 1, 'AMD', '', 'Socket AM4, 6-ядерный, 3600 МГц, Turbo: 4200 МГц, Matisse, Кэш L2 - 3 Мб, Кэш L3 - 32 Мб, 7 нм, 65 Вт', 0, 0, 1),
(3, 'Процессор Intel Core i5-10400 OEM', 6, 'CM8070104290715/CM8070104282718', 14700, 1, 'Intel', '', 'Socket 1200\r\n6-ядерный, 2900 МГц, Turbo: 4300 МГц, Comet Lake, Кэш L2 - 1.5 Мб, Кэш L3 - 12 Мб, 14 нм, 65 Вт', 0, 1, 1),
(4, 'Процессор Intel Core i3-10100 OEM', 6, 'CM8070104291317', 11100, 1, 'Intel', '', 'Socket 1200\r\n4-ядерный, 3600 МГц, Turbo: 4300 МГц, Comet Lake, Кэш L2 - 1.5 Мб, Кэш L3 - 12 Мб, 14 нм, 65 Вт', 0, 1, 1),
(5, 'Процессор AMD Ryzen 5 3500 OEM', 6, '100-000000050', 11000, 1, 'AMD', '', 'Socket AM4, 6-ядерный, 3600 МГц, Turbo: 4100 МГц, Matisse, Кэш L2 - 3 Мб, Кэш L3 - 16 Мб, 7 нм, 65 Вт', 0, 0, 1),
(6, 'Процессор AMD Ryzen 5 3600 OEM', 6, 'YD200GC6M2OFB', 4900, 1, 'AMD', '', 'Socket AM4, 2-ядерный, 3200 МГц, Raven Ridge, Кэш L2 - 1 Мб, Кэш L3 - 4 Мб, Radeon Vega 3, 14 нм, 35 Вт', 0, 0, 1),
(7, 'Процессор Intel Celeron G5905 OEM', 6, 'CM8070104292115', 3400, 0, 'Intel', '', 'Socket 1200, 2-ядерный, 3500 МГц, Comet Lake, Кэш L2 - 0.5 Мб, Кэш L3 - 4 Мб, Intel UHD Graphics 610, 14 нм, 58 Вт', 0, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `product_order`
--

CREATE TABLE `product_order` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_phone` varchar(255) NOT NULL,
  `user_comment` text NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `products` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`, `role`) VALUES
(1, 'Maksim', 'm@m.com', '123456', 'admin'),
(2, 'Maksim', 'a@a.com', '123456', ''),
(3, 'Maksim', 'b@a.com', '123456', ''),
(4, 'Maksim', 'c@a.com', '123456', '');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_order`
--
ALTER TABLE `product_order`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `product_order`
--
ALTER TABLE `product_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
