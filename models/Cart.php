<?php

/**
 * Cart model <br>
 * Модель для работы c корзиной
 */
class Cart
{
    /**
     * Adding product in cart <br>
     * Добавление товара в корзину
     * @param int $id <p>id товара</p>
     * @return int|mixed <p>Количество товаров в корзине</p>
     */
    public static function addProduct($id)
    {
        $id = intval($id);

        // key = product id, value = count of products <br>
        // ключ = id, занчение = кол-во
        $cartProducts = [];

        // Check cart for products <br>
        // Если в корзине уже есть товары
        if (isset($_SESSION['products'])) {
            $cartProducts = $_SESSION['products'];
        }

        // Is products exist <br>
        // Проверяем есть ли уже такой товар в корзине
        if (array_key_exists($id, $cartProducts)) {
            $cartProducts[$id]++;
        } else {
            $cartProducts[$id] = 1;
        }

        $_SESSION['products'] = $cartProducts;

        return self::countItems();
    }

    /**
     * Remove product from the cart <br>
     * Удаляет товар с указанным id из корзины
     * @param integer $id <p>id товара</p>
     */
    public static function removeProduct($id)
    {
        $id = intval($id);

        $cartProducts = [];

        // Check cart for products <br>
        // Если в корзине уже есть товары
        if (isset($_SESSION['products'])) {
            $cartProducts = $_SESSION['products'];
        }

        // Is products exist <br>
        // Проверяем есть ли уже такой товар в корзине
        if (array_key_exists($id, $cartProducts)) {
            if ($cartProducts[$id] <=1) {
                unset($cartProducts[$id]);
            } else {
                $cartProducts[$id]--;
            }
        }

        $_SESSION['products'] = $cartProducts;

        return self::countItems();
    }

    /**
     * Get count of items from the cart <br>
     * Подсчет количество товаров в корзине
     * @return int|mixed <p>Количество товаров в корзине</p>
     */
    public static function countItems()
    {
        if (isset($_SESSION['products'])) {
            $count = 0;

            foreach ($_SESSION['products'] as $id => $quantity) {
                $count = $count + $quantity;
            }

            return $count;
        } else {
            return 0;
        }
    }

    /**
     * Get array with ids and products count <br>
     * Возвращает массив с идентификаторами и количеством товаров в корзине
     * @return false|mixed
     */
    public static function getProducts()
    {
        if (isset($_SESSION['products'])) {
            return $_SESSION['products'];
        }

        return false;
    }

    /**
     * Get total price <br>
     * Получаем общую стоимость переданных товаров
     * @param array $products <p>Массив с информацией о товарах</p>
     * @return float|int
     */
    public static function getTotalPrice(array $products)
    {
        $productsInCart = self::getProducts();

        $total = 0;

        if ($productsInCart) {
            foreach ($products as $item) {
                $total += $item['price'] * $productsInCart[$item['id']];
            }
        }

        return $total;
    }

    /**
     * Cart clear <br>
     * Очистка корзины
     */
    public static function clear()
    {
        if (isset($_SESSION['products'])) {
            unset($_SESSION['products']);
        }
    }
}