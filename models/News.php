<?php

/**
 * Class News - the news feed <br>
 * Класс News - новостная лента
 */
class News
{
    /**
     * Getting news by id <br>
     * Возвращает новость с указанным id
     * @param integer $blogId
     * @return single news item with specified id
     */
    public static function getNewsItemById($id)
    {
        $id = intval($id);

        $db = Db::getConnection();

        if ($id) {
            $result = $db->query("select * from blog where id=".$id);
            $result->setFetchMode(PDO::FETCH_ASSOC);

            return $result->fetch();
        }
    }

    /**
     * Get news array <br>
     * Возвращает массив новостей
     * @return array <p>Массив с новостями</p>
     */
    public static function getNewsList()
    {
        $db = Db::getConnection();

        $blogNewsList = array();

        $result = $db->query("select * from blog");

        $i = 0;
        while ($row=$result->fetch()) {
            $blogNewsList[$i]['id'] = $row['id'];
            $blogNewsList[$i]['title'] = $row['title'];
            $blogNewsList[$i]['short_content'] = $row['short_content'];
            $blogNewsList[$i]['full_content'] = $row['full_content'];

            $i++;
        }

        return $blogNewsList;
    }
}