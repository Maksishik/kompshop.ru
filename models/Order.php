<?php

/**
 * Class Order - model for orders <br>
 * Класс Order - модель для работы с заказами
 */
class Order
{
    /**
     * Saving order <br>
     * Сохранение заказа
     * @param $userName <p>Имя</p>
     * @param $userPhone <p>Телефон</p>
     * @param $userComment <p>Комментарий</p>
     * @param $userId <p>id пользователя</p>
     * @param $productsInCart <p>Массив с товарами</p>
     * @param $code <p>код заказа</p>
     * @return bool <p>Результат выполнения метода</p>
     */
    public static function saveOrder($userName, $userPhone, $userComment, $userId, $productsInCart, $code)
    {
        $products = json_encode($productsInCart);
        $userId = !$userId ? null : $userId;

        $db = Db::getConnection();

        $sql = "insert into product_order(user_name, user_phone, user_comment, user_id, products, code, status) 
                values (:user_name, :user_phone, :user_comment, :user_id, :products, :code, 1)";

        $result = $db->prepare($sql);
        $result->bindParam('user_name'      ,$userName      );
        $result->bindParam('user_phone'     ,$userPhone     );
        $result->bindParam('user_comment'   ,$userComment   );
        $result->bindParam('user_id'        ,$userId        );
        $result->bindParam('products'       ,$products      );
        $result->bindParam('code'           ,$code          );

        return $result->execute();
    }

    /**
     * Generating order code <br>
     * Создание кода заказа
     * @return string <p>сгенерированный код</p>
     */
    public static function generateOrderNumber()
    {
        $today = date("Ymd");
        $rand = strtoupper(substr(uniqid(sha1(time())),0,4));
        $code = $today . $rand;

        return $code;
    }

    /**
     * Getting order list <br>
     * Возвращает список заказов
     * @return array <p>Список заказов</p>
     */
    public static function getOrdersList()
    {
        $db = Db::getConnection();

        $result = $db->query('SELECT id, user_name, user_phone, date, status FROM product_order ORDER BY id DESC');
        $ordersList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $ordersList[$i]['id'] = $row['id'];
            $ordersList[$i]['user_name'] = $row['user_name'];
            $ordersList[$i]['user_phone'] = $row['user_phone'];
            $ordersList[$i]['date'] = $row['date'];
            $ordersList[$i]['status'] = $row['status'];
            $i++;
        }

        return $ordersList;
    }
/*---------------------------------------Удалить----------------------------------------------------------*/
    /**
     * Возвращает текстое пояснение статуса для заказа :<br/>
     * <i>1 - Новый заказ, 2 - В обработке, 3 - Доставляется, 4 - Закрыт</i>
     * @param integer $status <p>Статус</p>
     * @return string <p>Текстовое пояснение</p>
     */
    public static function getStatusText($status)
    {
        switch ($status) {
            case '1':
                return 'Новый заказ';
                break;
            case '2':
                return 'В обработке';
                break;
            case '3':
                return 'Доставляется';
                break;
            case '4':
                return 'Закрыт';
                break;
        }
    }
/*-------------------------------------------------------------------------------------------------*/

    /**
     * Get order by id
     * Получаем данные о конкретном заказе
     * @param integer $id <p>id</p>
     * @return array <p>Массив с информацией о заказе
     * <br>
     * Orders array
     * </p>
     */
    public static function getOrderById($id)
    {
        $db = Db::getConnection();

        $sql = 'SELECT * FROM product_order WHERE id = :id';

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        $result->setFetchMode(PDO::FETCH_ASSOC);

        $result->execute();

        return $result->fetch();
    }

    /**
     * Removing order by id <br>
     * Удаляет заказ с заданным id
     * @param integer $id <p>id заказа</p>
     * @return boolean <p>Результат выполнения метода</p>
     */
    public static function deleteOrderById($id)
    {
        $db = Db::getConnection();

        $sql = 'DELETE FROM product_order WHERE id = :id';

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        return $result->execute();
    }

    /**
     * Editing order by id <br>
     * Редактирует заказ с заданным id
     * @param integer $id <p>id товара</p>
     * @param string $userName <p>Имя клиента</p>
     * @param string $userPhone <p>Телефон клиента</p>
     * @param string $userComment <p>Комментарий клиента</p>
     * @param string $date <p>Дата оформления</p>
     * @param integer $status <p>Статус <i>(включено "1", выключено "0")</i></p>
     * @return boolean <p>Результат выполнения метода</p>
     */
    public static function updateOrderById($id, $userName, $userPhone, $userComment, $date, $status)
    {
        $db = Db::getConnection();

        $sql = "UPDATE product_order
            SET 
                user_name = :user_name, 
                user_phone = :user_phone, 
                user_comment = :user_comment, 
                date = :date, 
                status = :status 
            WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':user_name', $userName, PDO::PARAM_STR);
        $result->bindParam(':user_phone', $userPhone, PDO::PARAM_STR);
        $result->bindParam(':user_comment', $userComment, PDO::PARAM_STR);
        $result->bindParam(':date', $date, PDO::PARAM_STR);
        $result->bindParam(':status', $status, PDO::PARAM_INT);

        return $result->execute();
    }

    /**
     * @param $userId
     * @return array
     */
    public static function getUsersOrderList($userId)
    {
        $id = intval($userId);

        $userOrders = array();

        if ($id) {
            $db  = Db::getConnection();

            $result = $db->query(  "select po.user_name, po.code, ot.name status, po.date 
                                            from product_order po
                                            join order_types ot on ot.id = po.status
                                            where user_id = '$id'");

            $i = 0;
            while ($row=$result->fetch()) {
                $userOrders[$i]['code'] = $row['code'];
                $userOrders[$i]['user_name'] = $row['user_name'];
                $userOrders[$i]['status'] = $row['status'];
                $userOrders[$i]['date'] = $row['date'];

                $i++;
            }
        }

        return $userOrders;
    }
}