<?php

/**
 * Class Product - model for products <br>
 * Класс Product - модель для работы с товарами
 */
class Product {
    // Default number of products displayed <br>
    // Количество отображаемых товаров по умолчанию
    const SHOW_BY_DEFAULT = 4;

    /**
     * Getting latest products <br>
     * Возвращает массив последних товаров
     * @param int $count <p>кол-во отображаемых товаров</p>
     * @return array <p>Массив с товарами</p>
     */
    public static function getLatestProducts($count = self::SHOW_BY_DEFAULT) {
        $count = intval($count);

        $db = Db::getConnection();

        $productsList = array();

        $result = $db->query("select * 
                                        from product 
                                        where status=1 
                                        order by id desc       
                                        limit $count");

        $i = 0;
        while ($row=$result->fetch()) {
            $productsList[$i]['id'] = $row['id'];
            $productsList[$i]['name'] = $row['name'];
            $productsList[$i]['image'] = $row['image'];
            $productsList[$i]['price'] = $row['price'];
            $productsList[$i]['is_new'] = $row['is_new'];
            $productsList[$i]['category_id'] = $row['category_id'];
            $productsList[$i]['brand'] = $row['brand'];

            $i++;
        }

        return $productsList;
    }

    /**
     * Getting products list by category id <br>
     * Возвращает список товаров в указанной категории
     * @param false $categoryId <p>id категории</p>
     * @param int $page [optional] <p>Номер страницы</p>
     * @return array <p>Массив с товарами</p>
     */
    public static function getProductsListByCategory($categoryId = false, $page = 1) {
        if ($categoryId) {
            $db = Db::getConnection();

            $products = array();

            $offset = $page == 1 ? 0 : (($page-1) * self::SHOW_BY_DEFAULT);

            $result = $db->query(  "select id, name, price, is_new, category_id from product "
                                            ."where status='1' and category_id='$categoryId' "
                                            ."order by id desc "
                                            ."limit ".self::SHOW_BY_DEFAULT
                                            ." offset ".$offset);

            $i = 0;
            while($row = $result->fetch()) {
                $products[$i]['id'] = $row['id'];
                $products[$i]['name'] = $row['name'];
                $products[$i]['price'] = $row['price'];
                $products[$i]['is_new'] = $row['is_new'];
                $products[$i]['category_id'] = $row['category_id'];

                $i++;
            }

            return $products;
        }
    }

    /**
     * Returning product by id <br>
     * Возвращает продукт с указанным id
     * @param $productId <p>id товара</p>
     * @return mixed <p>Массив с информацией о товаре</p>
     */
    public static function getProductById($productId) {
        $id = intval($productId);

        if ($id) {
            $db = Db::getConnection();

            $result = $db->query("select * from product where id=".$id);
            $result->setFetchMode(PDO::FETCH_ASSOC);

            return $result->fetch();
        }
    }

    /**
     * Returns total products in category <br>
     * Возвращаем количество товаров в указанной категории
     * @param $categoryId <p>id категории</p>
     * @return mixed <p>количество товаров</p>
     */
    public static function getTotalProductsInCategory($categoryId)
    {
        $db = Db::getConnection();

        $result = $db->query('select count(id) as count from product '
                                    . 'where status = 1 and category_id='.$categoryId);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $row = $result->fetch();

        return $row['count'];
    }

    /**
     * Returns products list by ids <br>
     * Возвращает список товаров с указанными индентификторами
     * @param array $productsIds <p>идентификаторы продуктов</p>
     * @return array <p>массив продуктов</p>
     */
    public static function getProductByIds(array $productsIds)
    {
        $products = [];

        $db = Db::getConnection();

        $idsStr = implode(',', $productsIds);

        $sql = "select * from product where status=1 and id in ($idsStr)";

        $result = $db->query($sql);
        $result->setFetchMode(PDO::FETCH_ASSOC);

        $i = 0;
        while ($row = $result->fetch()) {
            $products[$i]['id'] = $row['id'];
            $products[$i]['code'] = $row['code'];
            $products[$i]['name'] = $row['name'];
            $products[$i]['price'] = $row['price'];

            $i++;
        }

        return $products;
    }

    /**
     * Returning recommended products <br>
     * Возвращает список рекомендуемых товаров
     * @return array <p>Массив с товарами</p>
     */
    public static function getRecommendedProducts()
    {
        $db = Db::getConnection();

        $productsList = array();

        $result = $db->query("select * 
                                        from product 
                                        where is_recommended=1 
                                        order by id desc");

        $i = 0;
        while ($row=$result->fetch()) {
            $productsList[$i]['id'] = $row['id'];
            $productsList[$i]['name'] = $row['name'];
            $productsList[$i]['image'] = $row['image'];
            $productsList[$i]['price'] = $row['price'];
            $productsList[$i]['is_new'] = $row['is_new'];
            $productsList[$i]['category_id'] = $row['category_id'];
            $productsList[$i]['brand'] = $row['brand'];

            $i++;
        }

        return $productsList;
    }

    /**
     * Returning products list <br>
     * Возвращает список товаров
     * @return array <p>Массив с товарами</p>
     */
    public static function getProductsList()
    {
        $db = Db::getConnection();
        $productsList = array();

        $result = $db->query('select id, name, price, code from product order by id asc');

        $i = 0;
        while ($row = $result->fetch()) {
            $productsList[$i]['id'] = $row['id'];
            $productsList[$i]['name'] = $row['name'];
            $productsList[$i]['price'] = $row['price'];
            $productsList[$i]['code'] = $row['code'];
            $i++;
        }

        return $productsList;
    }

    /**
     * Remove product by id <br>
     * Удаляет товар с указанным id
     * @param integer $id <p>id товара</p>
     * @return boolean <p>Результат выполнения метода</p>
     */
    public static function deleteProductById($id)
    {
        $db = Db::getConnection();

        $sql = 'delete from product where id=:id';

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        return $result->execute();
    }

    /**
     * Adding new product <br>
     * Добавляет новый товар
     * @param array $options <p>Массив с информацией о товаре</p>
     * @return integer <p>id добавленной в таблицу записи</p>
     */
    public static function createProduct(array $options)
    {
        $db = Db::getConnection();

        $sql = 'INSERT INTO product '
            . '(name, code, price, category_id, brand, availability,'
            . 'description, is_new, is_recommended, status)'
            . 'VALUES '
            . '(:name, :code, :price, :category_id, :brand, :availability,'
            . ':description, :is_new, :is_recommended, :status)';

        $result = $db->prepare($sql);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':code', $options['code'], PDO::PARAM_STR);
        $result->bindParam(':price', $options['price'], PDO::PARAM_STR);
        $result->bindParam(':category_id', $options['category_id'], PDO::PARAM_INT);
        $result->bindParam(':brand', $options['brand'], PDO::PARAM_STR);
        $result->bindParam(':availability', $options['availability'], PDO::PARAM_INT);
        $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
        $result->bindParam(':is_new', $options['is_new'], PDO::PARAM_INT);
        $result->bindParam(':is_recommended', $options['is_recommended'], PDO::PARAM_INT);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);

        if ($result->execute()) {
            return $db->lastInsertId();
        }

        return 0;
    }

    /**
     * Editing product by id <br>
     * Редактирует товар с заданным id
     * @param integer $id <p>id товара</p>
     * @param array $options <p>Массив с информацей о товаре</p>
     * @return boolean <p>Результат выполнения метода</p>
     */
    public static function updateProductById($id, array $options)
    {
        $db = Db::getConnection();

        $sql = "UPDATE product
            SET 
                name = :name, 
                code = :code, 
                price = :price, 
                category_id = :category_id, 
                brand = :brand, 
                availability = :availability, 
                description = :description, 
                is_new = :is_new, 
                is_recommended = :is_recommended, 
                status = :status
            WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':code', $options['code'], PDO::PARAM_STR);
        $result->bindParam(':price', $options['price'], PDO::PARAM_STR);
        $result->bindParam(':category_id', $options['category_id'], PDO::PARAM_INT);
        $result->bindParam(':brand', $options['brand'], PDO::PARAM_STR);
        $result->bindParam(':availability', $options['availability'], PDO::PARAM_INT);
        $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
        $result->bindParam(':is_new', $options['is_new'], PDO::PARAM_INT);
        $result->bindParam(':is_recommended', $options['is_recommended'], PDO::PARAM_INT);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);

        return $result->execute();
    }

    /**
     * Returns products list by ids <br>
     * Возвращает список товаров с указанными индентификторами
     * @param array $idsArray <p>Массив с идентификаторами</p>
     * @return array <p>Массив со списком товаров</p>
     */
    public static function getProdustsByIds(array $productsIds)
    {
        $db = Db::getConnection();

        $idsString = implode(',', $productsIds);

        $sql = "SELECT * FROM product WHERE status='1' AND id IN ($idsString)";

        $result = $db->query($sql);

        $result->setFetchMode(PDO::FETCH_ASSOC);

        $i = 0;
        $products = array();
        while ($row = $result->fetch()) {
            $products[$i]['id'] = $row['id'];
            $products[$i]['code'] = $row['code'];
            $products[$i]['name'] = $row['name'];
            $products[$i]['price'] = $row['price'];
            $i++;
        }

        return $products;
    }

    /**
     * Returns image path <br>
     * Возвращает путь к изображению
     * @param integer $id
     * @return string <p>Путь к изображению</p>
     */
    public static function getImage($id)
    {
        // Product without image path <br>
        // Название изображения-пустышки
        $noImagePath = '/upload/images/no-image.jpg';

        // Path to products images <br>s
        // Путь к папке с товарами
        $path = '/upload/images/products/';

        // Path to product image <br>
        // Путь к изображению товара
        $pathToProductImage = $path . $id . '.jpg';

        // If image exists, returning image path <br>
        // Если изображение для товара существуетВозвращаем путь изображения товара
        if (file_exists($_SERVER['DOCUMENT_ROOT'].$pathToProductImage)) {
            return $pathToProductImage;
        }

        // or loading no image <br>
        // или путь к изображению-пустышки
        return $noImagePath;
    }
}
