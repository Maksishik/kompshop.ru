<?php

class User
{
    /**
     * User registration <br>
     * Регистрация пользователя
     * @param string $name <p>Имя</p>
     * @param string $email <p>E-mail</p>
     * @param string $password <p>Пароль</p>
     * @return boolean <p>Результат выполнения метода</p>
     */
    public static function registration($name, $email, $password)
    {
        $db = Db::getConnection();

        $sql = "insert into user (name, email, password, role) values (:name, :email, :password, '')";

        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);

        return $result->execute();
    }

    /**
     * User name validation <br>
     * Валидация имени
     * @param string $name <p>Имя</p>
     * @return boolean <p>Результат выполнения метода</p>
     */
    public static function checkName($name) {
        if (strlen($name) >= 2) {
            return true;
        }

        return false;
    }

    /**
     * User email validation <br>
     * Валидация почты
     * @param string $email <p>E-mail</p>
     * @return boolean <p>Результат выполнения метода</p>
     */
    public static function checkEmail($email) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }

        return false;
    }

    /**
     * User password validation <br>
     * Валидация пароля
     * @param string $password <p>Пароль</p>
     * @return boolean <p>Результат выполнения метода</p>
     */
    public static function checkPassword($password) {
        if (strlen($password) >= 6) {
            return true;
        }

        return false;
    }

    /**
     * Checking user email for uniqueness <br>
     * Проверяет не занят ли email другим пользователем
     * @param type $email <p>E-mail</p>
     * @return boolean <p>Результат выполнения метода</p>
     */
    public static function checkEmailExists($email)
    {
        $db = DB::getConnection();

        $sql = "select * from user where email = :email";

        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->execute();

        if ($result->fetchColumn())
            return true;

        return false;
    }

    /**
     * Checking user for exists <br>
     * Проверяем существует ли пользователь с заданными $email и $password
     * @param string $email <p>E-mail</p>
     * @param string $password <p>Пароль</p>
     * @return mixed : integer user id or false
     */
    public static function checkUserData($email, $password)
    {
        $db = Db::getConnection();

        $sql = "select * from user where email = :email and password = :password";

        $result = $db->prepare($sql);
        $result->bindParam('email', $email, PDO::PARAM_INT);
        $result->bindParam('password', $password, PDO::PARAM_INT);
        $result->execute();

        $user = $result->fetch();
        if ($user) {
            return $user['id'];
        }

        return false;
    }

    /**
     * Saving user id in session <br>
     * Запоминаем пользователя
     * @param integer $userId <p>id пользователя</p>
     */
    public static function auth($userId)
    {
        $_SESSION['user'] = $userId;
    }

    /**
     * Checking user exists in session <br>
     * Возвращает идентификатор пользователя, если он авторизирован.<br>
     * Иначе перенаправляет на страницу входа
     * @return string <p>Идентификатор пользователя</p>
     */
    public static function checkLogged()
    {
        if (isset($_SESSION['user'])) return $_SESSION['user'];

        header("Location: /user/login");
        exit;
    }

    /**
     * Checks if the user is a guest <br>
     * Проверяет является ли пользователь гостем
     * @return boolean <p>Результат выполнения метода</p>
     */
    public static function isGuest()
    {
        if (isset($_SESSION['user'])) {
            return false;
        }

        return true;
    }

    /**
     * Returns user by id <br>
     * Возвращает пользователя с указанным id
     * @param integer $id <p>id пользователя</p>
     * @return array <p>Массив с информацией о пользователе</p>
     */
    public static function getUserById($userId)
    {
        $id = intval($userId);

        if ($id) {
            $db  = Db::getConnection();

            $sql = "select * from user where id = :id";

            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);

            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();

            return $result->fetch();
        }
    }

    /**
     * Editing user data <br>
     * Редактирование данных пользователя
     * @param integer $id <p>id пользователя</p>
     * @param string $name <p>Имя</p>
     * @param string $password <p>Пароль</p>
     * @return boolean <p>Результат выполнения метода</p>
     */
    public static function edit($userId, $name, $password)
    {
        $db = Db::getConnection();

        $sql = "update user set name = :name, password = :password where id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $userId, PDO::PARAM_INT);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);

        return $result->execute();
    }

    /**
     * Phone number validation <br>
     * Проверяет телефон: не меньше, чем 10 символов
     * @param string $phone <p>Телефон</p>
     * @return boolean <p>Результат выполнения метода</p>
     */
    public static function checkPhone($userPhone)
    {
        if(preg_match("/^[0-9]{3}-[0-9]{4}-[0-9]{4}$/", $userPhone) ||
            strlen($userPhone) >= 10 ) {
            return true;
        }

        return false;
    }
}
