# Variables

#Database settings
DBHOST=localhost
DBNAME=kompshop
DBUSER=root
DBPASSWD=root

#Xdebug settings
XDEBUG_KEY=PHPSTORM
XDEBUG_HOST_IP=192.168.41.201
XDEBUG_PORT=9000

#Virtual host settings
VH_SERVERNAME=kompshop.ru
VH_DOCROOT=/var/www/kompshop.ru #
VH_DBFILE=$VH_DOCROOT/kompshop.sql #creation and filling db

echo -e "\n--- Installing now... ---\n"

echo -e "\n--- Updating packages list ---\n"
apt-get -qq update

# MySQL setup for development purposes
echo -e "\n--- Install MySQL specific packages and settings ---\n"
debconf-set-selections <<< "mysql-server mysql-server/root_password password $DBPASSWD"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $DBPASSWD"
debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password $DBPASSWD"
debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password $DBPASSWD"
debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password $DBPASSWD"
debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect none"
apt-get -y install mysql-server phpmyadmin >> /vagrant/vm_build.log 2>&1

echo -e "\n--- Setting up our MySQL user and db ---\n"
mysql -uroot -p$DBPASSWD -e "CREATE DATABASE $DBNAME" >> /vagrant/vm_build.log 2>&1
mysql -uroot -p$DBPASSWD -e "grant all privileges on $DBNAME.* to '$DBUSER'@'localhost' identified by '$DBPASSWD'" > /vagrant/vm_build.log 2>&1
echo -e "\n--- Fill database ---\n"
mysql -uroot -p$DBPASSWD -e "use $DBNAME; source $VH_DBFILE;" >> /vagrant/vm_build.log 2>&1

#PHP install
echo -e "\n--- Installing PHP packages ---\n"
apt-get -y install php apache2 libapache2-mod-php php-curl php-gd php-mysql php-gettext >> /vagrant/vm_build.log 2>&1

echo -e "\n--- Installing PHP-xdebug ---\n"
apt-get -y install php-xdebug >> /vagrant/vm_build.log 2>&1

echo -e "\n--- Xdebug config (before 3.0V) ---\n"
echo "zend_extension=xdebug.so
      xdebug.remote_enable=true
      xdebug.remote_connect_back=true
      xdebug.idekey=$XDEBUG_KEY
      xdebug.remote_autostart=true
      xdebug.remote_host=$XDEBUG_HOST_IP
      xdebug.remote_port=$XDEBUG_PORT" > /etc/php/7.0/mods-available/xdebug.ini

echo -e "\n--- Add kompshop.ru to available apache sites ---\n"
echo "<VirtualHost *:80>
              ServerName $VH_SERVERNAME
              ServerAdmin webmaster@localhost
              DocumentRoot $VH_DOCROOT
              ErrorLog ${APACHE_LOG_DIR}/error.log
              CustomLog ${APACHE_LOG_DIR}/access.log combined
      </VirtualHost>" > /etc/apache2/sites-available/$VH_SERVERNAME.conf

a2ensite $VH_SERVERNAME >> /vagrant/vm_build.log 2>&1

echo -e "\n--- Enabling mod-rewrite ---\n"
a2enmod rewrite >> /vagrant/vm_build.log 2>&1

echo -e "\n--- Allowing Apache override to all ---\n"
sed -i "s/AllowOverride None/AllowOverride All/g" /etc/apache2/apache2.conf

echo -e "\n--- We definitely need to see the PHP errors, turning them on ---\n"
sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php/7.0/apache2/php.ini
sed -i "s/display_errors = .*/display_errors = On/" /etc/php/7.0/apache2/php.ini

echo -e "\n--- Restarting Apache ---\n"
service apache2 restart >> /vagrant/vm_build.log 2>&1

echo -e "\n--- Init complete ---\n"