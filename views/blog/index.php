<?php include ROOT.'/views/layouts/header.php'; ?>

<?php foreach ($newsList as $newsListItem): ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a href="/blog/<?php echo $newsListItem['id']; ?>">
                    <?php echo $newsListItem['title']; ?>
                </a>
            </h4>
        </div>
    </div>
<?php endforeach; ?>

<?php include ROOT.'/views/layouts/footer.php'; ?>
