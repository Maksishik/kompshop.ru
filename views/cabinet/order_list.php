<?php require_once ROOT . '/views/layouts/header.php'; ?>

    <section>
        <div class="container">
            <div class="row">
                <h1>Спиок заказов</h1>

                <table class="table-bordered table-striped table">
                    <tr>
                        <th>Номер заказа</th>
                        <th>Клиент</th>
                        <th>Статус</th>
                        <th>Стомость</th>
                        <th>Дата</th>
                    </tr>
                    <?php foreach ($result as $product): ?>
                        <tr>
                            <td><?php echo $product['code'];?></td>
                            <td>
                                <?php echo $product['user_name'];?>
                            </td>
                            <td>
                                <?php echo $product['status'];?>
                            </td>
                            <td>
                                <?php echo 0;?>
                            </td>
                            <td>
                                <?php echo $product['date'];?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </section>

<?php require_once ROOT . '/views/layouts/footer.php'; ?>