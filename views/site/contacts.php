<?php include ROOT . '/views/layouts/header.php'; ?>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4 padding-right">
                    <?php if ($result): ?>
                        <h2>Сообщение было отправлено! Мы ответим вам на указанный email.</h2>
                    <?php else: ?>
                        <?php if (isset($errors) && is_array($errors)): ?>
                            <ul>
                                <?php foreach ($errors as $error): ?>
                                    <li> - <?php echo $error; ?></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>

                        <div class="signup-form">
                            <h2>Обратная свяь</h2>
                            <h5>Есть вопросы? Напишите нам</h5>
                            <form action="#" method="post">
                                <label for="userEmail">Ваша почта</label>
                                <input id="userEmail" type="text" name="email" placeholder="Адрес почты" value="<?php echo $userMail;?>">
                                <label for="msg">Сообщение</label>
                                <input id="msg" type="text" name="msg" placeholder="Сообщение" value="<?php echo $userMsg;?>">
                                <button type="submit" name="submit" class="btn-btn-default">Отправить</button>
                            </form>
                        </div>
                    <?php endif; ?>
                    <br><br>
                </div>
            </div>
        </div>
    </section>

<?php include ROOT . '/views/layouts/footer.php'; ?>